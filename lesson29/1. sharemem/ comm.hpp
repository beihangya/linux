#ifndef   __COMM_HPP__     
#define __COMM_HPP__   
//如果在该文件之前未定义 __COMM_HPP__  宏，那么 #define __COMM_HPP__ 将会定义 __COMM_HPP__     

       #include <sys/ipc.h>
       #include <sys/shm.h>
#include <iostream>
#include<string>
#include"log.hpp"
  #include <string.h>
#include<cstdlib>
   #include <sys/types.h>
       #include <sys/stat.h>

using namespace std;
const string pathname="/home/cxq";
const int proj_id = 0x6666;
// 共享内存的大小一般建议是4096的整数倍
// 4097,实际上操作系统给你的是4096*2的大小
const int size = 4097; 
Log log ;
key_t GetKey()
{
      key_t k=ftok(pathname.c_str() ,proj_id);
      //失败
      if(k <0)
      {
         log(Fatal, "ftok error:  %s", strerror(errno));
        exit(1);
      }
      //成功
        log(Info, "ftok success, key is : 0x%x", k);
   return k ;
}


//创建共享内存
int GetShareMemHelper(int flag)
{
     key_t k = GetKey();
    int shmid = shmget( k ,size, flag); //size是共享内存的大小

  if(shmid < 0)
    {
            log(Fatal, "create share memory error: %s", strerror(errno) );
         exit(2);
    }

  log(Info, "create share memory success, shmid: %d",shmid );

  return shmid;
}



//一个进程创建共享内存，一个进程获取共享内存
int CreateShm()
{
    return GetShareMemHelper(IPC_CREAT | IPC_EXCL | 0666);
}

int GetShm()
{
    return GetShareMemHelper(IPC_CREAT); 
}

#define FIFO_FILE "./myfifo"
#define MODE 0664

enum
{
    FIFO_CREATE_ERR = 1,
    FIFO_DELETE_ERR,
    FIFO_OPEN_ERR
};

class Init
{
public:
    Init()
    {
        // 创建管道
        int n = mkfifo(FIFO_FILE, MODE);
        if (n == -1)
        {
            perror("mkfifo");
            exit(FIFO_CREATE_ERR);
        }
    }
    ~Init()
    {

        int m = unlink(FIFO_FILE);
        if (m == -1)
        {
            perror("unlink");
            exit(FIFO_DELETE_ERR);
        }
    }
};



#endif