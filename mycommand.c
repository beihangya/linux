#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h> 
int main()
{
  pid_t id = fork(  ) ;
  if(id ==0 )
  {
    //child 
    
printf( " before : I am a process , pid %d , ppid %d\n", getpid () , getppid() ) ;
sleep(5);
//最后一个参数必须是NULL
execl ( "/usr/bin/ls" , "ls" , "-a","-l", NULL );  
printf( " after : I am a process , pid %d , ppid %d\n", getpid () , getppid() ) ;
    exit(0);
  }
  //father 
  pid_t ret = waitpid(id , NULL, 0);
  if(ret > 0 )
  {
    printf("wait success, father pid :%d  , ret id:%d\n", getpid() , ret);
  }
  sleep(5);
  return 0 ;
}
