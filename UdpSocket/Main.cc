
#include"UdpServer.hpp"
#include<memory>
#include"Log.hpp"


void Usage(std::string proc)
{
    std::cout << "\n\rUsage: " << proc << " port[1024+]\n" << std::endl;
}


// std::string Handler(const std::string &info, const std::string  &  clientip ,uint16_t clientport     )
// {
//     std::cout<<"["<<clientip<<":"<<clientport<<"]#"<<info<<std::endl;
//     std::string res = "Server get a message: ";
//     res += info;
//     std::cout << res << std::endl;

//     // pid_t id = fork();
//     // if(id == 0)
//     // {
//     //     // ls -a -l -> "ls" "-a" "-l"
//     //     // exec*();
//     // }
//     return res;
// }

// std::string ExcuteCommand(const std::string &cmd)
// {
//     // SafeCheck(cmd);


//     FILE *fp = popen(cmd.c_str(), "r");   //父进程可以从子进程的标准输出读取数据
//     if(nullptr == fp)
//     {
//         perror("popen");
//         return "error";
//     }
//     std::string result;
//     char buffer[4096];
//     while(true)
//     {
//         char *ok = fgets(buffer, sizeof(buffer), fp);
//         if(ok == nullptr) break;
//         result += buffer;
//     }
//     pclose(fp);

//     return result;
// }



int main(int argc , char *argv[])
{
if(argc!= 2) 
{
   Usage(argv[0]);
        exit(0);
}
//  argc 为2 
 uint16_t port = std::stoi(argv[1] ) ;
std::unique_ptr<UdpServer> svr(new UdpServer(port));
     
    svr->Init() ;
    svr->Run() ;
    return 0 ;
}