#pragma once 
#include<iostream>
 #include <stdio.h>
#include<cerrno>
 #include <unistd.h>
 #include <sys/types.h>
 #include <sys/stat.h>
#include <fcntl.h>
#include<string> 
#define  FIFO_FILE   "./myfifo"
#define MODE  0664


enum
{
    FIFO_CREATE_ERR = 1,
    FIFO_DELETE_ERR,
    FIFO_OPEN_ERR
};

class Init 
{
 public:
 Init( )
 {
  //创建管道文件
    int n = mkfifo ( FIFO_FILE ,MODE);
    if(n==-1)
    {
        perror ("mkfifo fail\n");
        exit(FIFO_CREATE_ERR);
    }

 }

 ~Init()
 {
 //删除管道
   int m = unlink(FIFO_FILE);
    if(m==-1)
    {
        perror ("unlink fail\n");
        exit(FIFO_DELETE_ERR);
    }

 }

};