#include "HttpServer.hpp"
#include <iostream>
#include <memory>
#include <pthread.h>
#include "Log.hpp"
#include<string>
using namespace std;
int main(int argc ,const char * argv[])
{ 
     //./HttpServer  8080
    if(argc != 2 )
    {
        exit(1) ;
    }
    //argc为2 
    uint16_t port = std::stoi(argv[1] );
    std::unique_ptr<HttpServer> svr(new HttpServer (port)) ;
     svr->Start();

    return 0 ;
}