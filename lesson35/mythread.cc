#include<iostream>
#include <pthread.h>
#include<string>
#include<unistd.h>
#include<cstdlib>
using namespace std ;




// void show(const string &name)
// {
//     cout << name << "say# "<< "hello thread" << endl;
// }
// void * threadRoutine(void * args)
// {
//     //新创建的线程获取主线程传的参数"Thread 1"
//     const char * name  = (const char *) args;   
//    while(true)
//    {
//   //  cout << "new thread, pid: " << getpid() << endl;
//     //show("[new thread]");
//     printf("%s\n" , name); 
//     sleep(1);
//    }
// }

// int main()
// {
//    pthread_t tid ;
//    //创建线程
//    pthread_create(&tid ,nullptr , threadRoutine, (void *)"Thread 1");  // threadRoutine 是函数指针

//   while(true)
//    {
//   //  cout << "main thread, pid: " << getpid() << endl;
//       //show("[new thread]");
//       printf("main thread %p\n", tid);
//     sleep(1);
//    }
// }





// void * threadRoutine(void * args)
// {
//     //新创建的线程获取主线程传的参数"Thread 1"
//     const char * name  = (const char *) args;   
//     int  cnt =5 ;
//    while(true)
//    {
   
//   //  cout << "new thread, pid: " << getpid() << endl;
//     printf("%s\n" , name); 
//     sleep(1);



//     cnt --;
//     if(cnt ==0)
//         break; 
//      }
//   //终止线程
//  pthread_exit(  (void*)  200) ;

//      //(void*)1 ，将1的地址强制转换为void * 
//     // return (void*)1 ; //代码运行到这里 ，新创建的线程就退出了
// }

// int main()
// {
//    pthread_t tid ;
//    //创建线程
//    pthread_create(&tid ,nullptr , threadRoutine, (void *)"Thread 1");  // threadRoutine 是函数指针


//     //主线程等待，是阻塞等待的 
//     //sleep(7);
//     void * retval;
//     pthread_join(tid,&retval) ;  //retval 拿到新创建的线程的返回值
    
//   cout << "main thread quit ..., ret: " <<(long long int)retval<< endl;
// }





// void * threadRoutine(void * args)
// {
//     //新创建的线程获取主线程传的参数"Thread 1"
//     const char * name  = (const char *) args;   
//     int  cnt =5 ;
//    while(true)
//    {
   
//   //  cout << "new thread, pid: " << getpid() << endl;
//     printf("%s\n" , name); 
//     sleep(1);



//     cnt --;
//     if(cnt ==0)
//         break; 
//      }

// }

// int main()
// {
//    pthread_t tid ;
//    //创建线程
//    pthread_create(&tid ,nullptr , threadRoutine, (void *)"Thread 1");  // threadRoutine 是函数指针
//     sleep(1) ;//等待1s，给时间让新线程启动
    
// pthread_cancel(tid); //主线程取消新创建的线程 ，主线程继续往后走 ，新线程已经退出

//     //主线程等待，是阻塞等待的 
//     //sleep(7);
//     void * retval;
//     pthread_join(tid,&retval) ;  //retval 拿到新创建的线程的返回值
    
//   cout << "main thread quit ..., ret: " <<(long long int)retval<< endl;
// }


class Request
{
public:
    Request(int start, int end, const string &threadname)
    : start_(start)
    , end_(end)
    , threadname_(threadname)
    {}
public:
    int start_;
    int end_;
    string threadname_;
};

class Response
{
public:
    Response(int result, int exitcode)
    :result_(result)
    ,exitcode_(exitcode)
    {}
public:
    int result_;   // 计算结果
    int exitcode_; // 计算结果是否可靠
};

void * sumCount(void * args)//线程的参数和返回值，不仅仅可以用来进行传递一般参数，也可以传递对象！！
{
  Request * rq = ( Request *)args;

Response * rsp =new Response (0,0);

  
  for( int i =rq->start_ ; i<=rq->end_ ; i++)
  {
     cout << rq->threadname_ << " is runing, caling..., " << i << endl;
        rsp->result_+=i;
      usleep(100000);
  }

  delete  rq ;  

  return rsp;
}

int main()
{
  pthread_t tid ;
Request *rq = new Request(1,100,"thread 1");



  //创建线程
  pthread_create(&tid, nullptr , sumCount, rq) ;
  //线程等待
  void * ret ;
  pthread_join(tid, &ret);  //ret 拿到新创建的线程的返回值

Response * rsp = (Response *)ret ;
 cout << "rsp->result: " << rsp->result_ << ", exitcode: " << rsp->exitcode_ << endl;


delete rsp;

return 0 ;
}