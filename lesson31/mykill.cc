#include<iostream>
#include<stdio.h>
 #include <signal.h>
 #include<unistd.h>
  #include <stdlib.h>
  #include<string>

using namespace std ;


void myhandler(int signo)
{
    cout << "process get a signal: " << signo <<endl;
  //   exit(1);
}
// int main()
// {
//    //signal(2 , myhandler);//myhandler是函数指针
//    //signal只需要设置一次，往后都有效
//    //信号的产生和我们自己的代码的运行时异步的 ,异步：代码在运行时，信号什么时候产生是不清楚的，代码不用管信息什么时候产生，代码继续运行就可以了
//    //signal(3 , myhandler);//myhandler是函数指针


//    signal(19 , myhandler);//myhandler是函数指针


//  while(true)
//     {
//         cout << "I am a crazy process : " << getpid() << endl;
//         sleep(1);
//     }


//     return 0 ;
// }


void Usage(string proc)
{
    cout << "Usage:\n\t" << proc << " signum pid\n\n";
}


//   mykill signum pid


// int main(int argc ,char* argv[]) //命令行参数
// {
//    //告诉用户如何使用
//   if(argc !=3)
//   {
//      Usage(argv[0]);
//          exit(1);
//   }

//         for( int i = 0; i!=3 ; i++)
//         {
//             cout<<"argv["<<i<<"]"<<argv[i]<<" " ;
//         }

//  string signumStr(argv[1]);
//     string pidStr(argv[2]);

//    int signum = stoi(signumStr); 
//    pid_t pid = stoi(pidStr);

//      int n =kill(pid,signum);
//     if(n == -1)
//     {   
//         perror("kill");
//         exit(2);
//     }
//     return 0 ;
// }


int main()
{
    int cnt =5 ;
    signal(2,myhandler);
     while (true)
    {
        cout << "I am a process, pid: " << getpid() << endl;
        sleep(1);
        cnt--;
        if(cnt== 0) 
        {
            raise(2); 
        }
    }
    return 0 ;
}