#include<stdio.h>
#include<unistd.h>
#include<string.h>
int main()
{
  const char * fstr = "hello fwrite\n ";
  const char * str =" hello write\n";
  //C
  printf("hello printf\n "); //stdout ->1
  fprintf ( stdout ,"hello fprintf\n " );//stdout ->1
  fwrite( fstr , strlen(fstr), 1, stdout);
  //操作系统提供的系统调用接口
 write(1, str, strlen(str));
   close(1);
//  fork();
  return 0 ;
}
