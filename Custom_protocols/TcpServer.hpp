#pragma once
#include "Log.hpp"
#include "Socket.hpp"
#include <functional>

// func_t 是一个可以存储任何接受一个 std::string 引用并返回一个 std::string 的可调用对象的类型
using func_t = std::function<std::string(std::string &package)>;

class TcpServer
{
public:
    TcpServer(uint16_t port, func_t callback) : port_(port), callback_(callback)
    {
    }
    ~TcpServer()
    {
    }

public:
    bool InitServer()
    {
        listensock_.Socket();
        listensock_.Bind(port_);
        listensock_.Lisenten();
        lg(Info, "init server .... done");
        return true;
    }
    void Start()
    {
        signal(SIGCHLD, SIG_IGN);
        signal(SIGPIPE, SIG_IGN);
        while (true)
        {
            std::string clientip;
            uint16_t clientport;
            int sockfd = listensock_.Accept(&clientip, &clientport);
            if (sockfd < 0)
            {
                // 重新获取连接
                continue;
            }
            lg(Info, "accept a new link, sockfd: %d, clientip: %s, clientport: %d", sockfd, clientip.c_str(), clientport);
            // 提供服务
            if (fork() == 0)
            {
                listensock_.Close();
                // 数据计算

                std::string inbuffer_stream;
                while (true)
                {
                    char buffer[128];
                    int n = read(sockfd, buffer, sizeof(buffer));
                    if (n > 0)
                    {
                        buffer[n] = 0; // 将buffer当成字符串处理
                        inbuffer_stream += buffer;
                        lg(Debug, "debug:\n%s", inbuffer_stream.c_str());
                        //对inbuffer_stream进行解析
                        std::string info = callback_(inbuffer_stream);
                         lg(Debug, "debug, response:\n%s", info.c_str());
                        if (info.empty())
                            continue;

                        write(sockfd, info.c_str(), info.size());
                    }
                    // 如果客户端退出 ，服务器就会读到0
                    else if (n == 0)
                        break;
                    else
                        break;
                }
                exit(0);
            }
            close(sockfd);
        }
    }

private:
    uint16_t port_;
    Sock listensock_;
    func_t callback_; // 回调
};