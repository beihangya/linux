#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
int g_val = 100;
int main()
{
  pid_t id = fork();
  if(id ==0 )
  {
  //child
   int count = 5 ;
   while(1)
   {
     printf( " child :  pid:%d  ppid:%d   g_val:%d   &g_val:%p\n", getpid() ,getppid() , g_val , &g_val);
     sleep(1);
     if(count )//count不为0
     {
       count--;
     }
     else //count为0
     {
       g_val=200;
       printf(" 子进程change g_val : 100 ->200\n");
       count--; 
     }
   }
  }
  else 
  {

   while(1)
   {
     printf( " parent :  pid:%d  ppid:%d  g_val: %d &g_val: %p\n", getpid() ,getppid() , g_val , &g_val);
     sleep(1);
   }
  }
  return 0 ;
}
