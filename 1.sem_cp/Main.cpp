#include<iostream>
#include"RingQueue.hpp"
#include<pthread.h>
#include<ctime>
#include<unistd.h>
#include<string>
#include"Task.cpp"
using namespace std ;




struct ThreadData
{
    RingQueue<int> *rq;
    std::string threadname; //线程名
};
void * Producter (void * args) //生产者
{
   sleep(3) ;
 ThreadData * td =( ThreadData *) args;
   RingQueue<int> * rq = td->rq;
     std::string name = td->threadname;
 while (true) 
    {
        // 1. 获取数据
     int data = rand()%10 +1 ; //1到10的随机数
      rq->Push(data) ;
      cout<<"Producter  data done  ,data is " <<data <<" who "<<name<<endl;
      sleep(1) ;
    }
 return nullptr ;
}

void *Consumer (void * args) //消费者
{
  ThreadData * td =( ThreadData *) args;
   RingQueue<int> * rq = td->rq;
     std::string name = td->threadname;
  while (true)
    {
      //消费数据
       int data =0 ;
       rq->Pop(&data) ;
      cout<<"Consumer get data ,data is " <<data <<" who "<<name<<endl;
    }
 return nullptr ;
}

int main()
{


     RingQueue<int> *rq   =new RingQueue<int>();

   //   //创建单个生产者 和 单个消费者模型
   //   pthread_t p ;
   //   pthread_t c ;
   //      //基于环形队列的生产消费模型
   //   pthread_create(&p,nullptr , Producter , rq);
   //   pthread_create(&c,nullptr , Consumer , rq);
   //  //线程等待 
   //  pthread_join(c,nullptr);
   //  pthread_join(p,nullptr);




   //创建多个生产者 和 多个消费者模型
     pthread_t c[5] ;
     pthread_t p[3] ;
        //基于环形队列的生产消费模型
   for(int i =0 ; i <5 ; i++)
   {
       ThreadData * td = new ThreadData;
      td->rq = rq;
    td->threadname = "Consumer-" + std::to_string(i);
    
         pthread_create(c+i,nullptr , Consumer , td);
   }
       for(int i =0 ; i <3 ; i++)
   {
      ThreadData * td = new ThreadData;
      td->rq = rq;
    td->threadname = "Productor-" + std::to_string(i);

         pthread_create(p+i,nullptr ,Producter  , td);
   }
    //线程等待 
           for(int i =0 ; i <5 ; i++)
   {
      
         pthread_join(c[i],nullptr );
   }
             for(int i =0 ; i <3 ; i++)
   {
          pthread_join(p[i],nullptr );
   }

    return 0 ;
}