//防止头文件重复包含
#ifndef __MYSTDIO_H__
#define __MYSTDIO_H__ 
#define SIZE 1024


#define FLUSH_NOW 1  // 无缓冲
#define FLUSH_LINE 2 //行缓冲
#define FLUSH_ALL 4//全缓冲
#include<string.h> 

typedef struct IO_FILE
{
  int fileno ;// 文件描述符fd
  int  flag ; //缓冲方式
 //  char inbuffer[SIZE] ;//接收缓冲区 
  char outbuffer[SIZE] ; // 
  //  int in_pos ; 
  int out_pos; //outbuffer这个缓冲区使用了多少，由out_pos决定
}_FILE;

 _FILE *  _fopen ( const char * filename, const char * mode ) ;
  int  _fwrite (  _FILE * fp , const char  * ptr , int len  ) ;
  void  _fclose(_FILE* fp  )  ;
#endif
