#include<iostream>
#include"RingQueue.hpp"
#include<pthread.h>
#include<ctime>
#include<unistd.h>
using namespace std ;

void * Producter (void * args) //生产者
{
   sleep(3) ;
  RingQueue<int> * rq =  ( RingQueue<int> * )args;
 while (true)
    {
        // 1. 获取数据
     int data = rand()%10 +1 ; //1到10的随机数
      rq->Push(data) ;
      cout<<"Producter  data done  ,data is " <<data <<endl;
      sleep(1) ;
    }
 return nullptr ;
}

void *Consumer (void * args) //消费者
{
  RingQueue<int> * rq =( RingQueue<int> *) args;
  while (true)
    {
      //消费数据
       int data =0 ;
       rq->Pop(&data) ;
      cout<<"Consumer get data ,data is " <<data <<endl;
    }
 return nullptr ;
}

int main()
{


     RingQueue<int> *rq   =new RingQueue<int>();

     //创建单个生产者 和 单个消费者模型
     pthread_t p ;
     pthread_t c ;
        //基于环形队列的生产消费模型
     pthread_create(&p,nullptr , Producter , rq);
     pthread_create(&c,nullptr , Consumer , rq);
    //线程等待 
    pthread_join(c,nullptr);
    pthread_join(p,nullptr);


    return 0 ;
}