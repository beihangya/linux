#include "Socket.hpp"
#include <time.h>
#include <unistd.h>
#include "Protocol.hpp"
#include <cassert>
static void Usage(const std::string &proc)
{
    std::cout << "\nUsage: " << proc << " serverip serverport\n"
              << std::endl;
}
//./client  ip port
int main(int argc, char *argv[])
{

    if (argc != 3)
    {
        Usage(argv[0]);
        exit(0);
    }
    Sock sockfd;
    sockfd.Socket(); // 创建套接字
    std::string serverip = argv[1];
    uint16_t serverport = std::stoi(argv[2]);
    bool r = sockfd.Connect(serverip, serverport);
    if (!r) // 连接失败
    {
        return 1;
    }
    srand(time(nullptr) ^ getpid());
    const std::string opers = "+-*/%=-=&^";
    std::string inbuffer_stream;
    int cnt =10;
    while (cnt)
    {
        int x = rand() % 100 + 1;
        usleep(1234);
        int y = rand() % 100;
        usleep(4321);
        char op = opers[rand() % opers.size()];
        Request req(x, y, op);
        req.DebugPrint();

        //将请求根据协议发送给服务器
        //1、将请求序列化 2、添加报头3、


        std::string package;
        req.Serialize(&package); // 序列化

        package = Encode(package); // 使用自定义协议添加报头，封装数据
        //将请求写到服务器
         int n = write(sockfd.Fd(), package.c_str(), package.size());
        std::cout << "这是最新的发出去的请求: " << n << "\n" << package;
        n = write(sockfd.Fd(), package.c_str(), package.size());
        std::cout << "这是最新的发出去的请求: \n" << n << "\n" << package;
        n = write(sockfd.Fd(), package.c_str(), package.size());
        std::cout << "这是最新的发出去的请求: \n" << n << "\n" << package;
        n = write(sockfd.Fd(), package.c_str(), package.size());
        std::cout << "这是最新的发出去的请求: \n" << n << "\n" << package;
   
        // 无法保证能读到一个完整的报文
        char buffer[128];
        //服务器处理完客户端发送的数据 ，客户端读取服务器的响应
        //客户端读取服务器的响应时，也无法做到读取一个完整的报文 
        //客户端读取服务器的响应，并将响应作解析 
         n = read(sockfd.Fd(), buffer, sizeof(buffer)); // 将sockfd的数据读到buffer中
       
        // if (n > 0)
        // {
        //     buffer[n] = 0; // 把buffer当字符串处理

        //     // 获得有效载荷
        //     std::string content;
        //     inbuffer_stream += buffer;
        //     bool r = Decode(inbuffer_stream, &content);//将响应作解析 
        //     assert(r); // r==false 就会报错

        //     // 构建响应
        //     Response resp;

        //     r = resp.DeSerialize(content);  //反序列化
        //     assert(r); // r==false 就会报错
        //     resp.DebugPrint();
        //     cnt --;
        // }
        // sleep(1) ;
        std::cout << "=================================================" << std::endl;
    }
    sockfd.Close();
    return 0;
}