#pragma once

#include <iostream>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include<cstring>
    #include <arpa/inet.h>
#include<unistd.h>

       #include <sys/wait.h>
#include<pthread.h>
#include"ThreadPool.hpp"
#include"Log.hpp"
#include"Task.hpp"
#include<signal.h>
#include"Daemon.hpp"
const int defaultfd = -1;
const uint16_t defaultport = 8888;

const std::string defaultip = "0.0.0.0";
const int backlog =10 ;
 extern  Log lg ;


enum
{
    UsageError = 1,
    SocketError,
    BindError,
    ListenError,
};
class TcpServer;
class ThreadData
{

  public:
  ThreadData( int fd ,const std::string  & ip , const  uint16_t  & port ,  TcpServer * t  ):sockfd(fd ) ,clientip(ip) ,clientport(port),tsvr(t)
  { 

  }
  public:
  int sockfd ; 
  std::string clientip ;
  uint16_t clientport ;  
  TcpServer * tsvr ; 
};
class TcpServer
{
public:
    TcpServer(const uint16_t &port = defaultport, const std::string &ip = defaultip)
        : listensock_(defaultfd), port_(port), ip_(ip)
    {
    }
    void Init()
    {
        listensock_ = socket(AF_INET, SOCK_STREAM, 0); // IPv4
        if(listensock_ < 0 ) 
        {
            lg(Fatal , "create listensock_ ,errno : %d errstring  : %s" , errno , strerror(errno) );
            exit(SocketError ) ;
        }
         lg(Info , "create listensock_  success %d" , listensock_);

    
        struct sockaddr_in local ;
        memset(&local ,0 ,sizeof (local));
        local.sin_family = AF_INET ;
        local.sin_port = htons(port_) ;
        inet_aton(ip_.c_str() , &(local.sin_addr ) );
           // local.sin_addr.s_addr = INADDR_ANY;
        if (   bind ( listensock_ , ( struct sockaddr *)&local , sizeof (local)) < 0) 
        {
            lg( Fatal , "bind error , error : %d , errstring  : %s " , errno, strerror(errno));
            exit(BindError);
        }

        lg( Info , "bind   listensock_ success , sockfd  %d " , listensock_ ) ; 

        //监听
        if( listen(listensock_ , backlog)  < 0 )
        {
                 lg( Fatal , "listen error , error : %d , errstring  : %s " , errno, strerror(errno));
            exit(ListenError);
        }

           lg( Info , "listen   listensock_ success , sockfd  %d " , listensock_ ) ; 

    }


    void Start ()
    {

      
        //守护进程
        Daemon();
        //问题：    写端是正常写入，读端关闭了。操作系统就要会发送13号信号，杀掉正在写入的进程 ，这种情况在网路里也是存在的
        //  signal ( SIGPIPE, SIG_IGN) 是为了解决这个问题 
        signal ( SIGPIPE, SIG_IGN) ;  //忽略13号信号  
        //启动线程池
        ThreadPool<Task>::GetInstance()->Start() ;

        lg( Info , " tcp is running ") ;

        for( ; ; )
        {
            //获取新连接

            struct sockaddr_in client ;
            socklen_t   len =   sizeof(client);
          int sockfd =   accept(listensock_  ,(struct sockaddr *)&client ,&len ) ;
        if( sockfd < 0)   //accept出错，继续下一个
        {
            lg(Warning , "accept error , error : %d , errstring  : %s " , errno, strerror(errno));
            continue ; 
        }
            uint16_t clientport = ntohs(client.sin_port );
            char clientip[32] ;

            inet_ntop(AF_INET , &(client.sin_addr), clientip ,sizeof(clientip) ) ; //将整数IP转换成字符串IP

         lg( Info , "link   socket success , sockfd  %d  ipstr : %s , clientport  : %d" , sockfd , clientip,  clientport) ; 


                //线程池版本
                        Task t (sockfd , clientip ,clientport) ;
                    ThreadPool<Task>::GetInstance()->Push(t);
        }


    }





    // }
    ~TcpServer()
    {
    }

private:
    int listensock_;
    uint16_t port_;  // 端口
    std::string ip_; // ip
};