#pragma once 

#include<iostream>
#include<unordered_map>
#include <fstream>
#include"Log.hpp"
extern  Log lg ;
const std::string dictname = "./dict.txt";
const std::string sep = ":";


 //yellow:黄色...
    bool  Split(std::string  & s , std::string  * part1,std::string  *part2 )
    {
        auto pos = s.find(sep) ;
        //没找到
        if(pos == std::string::npos) return false ;
        //找到了
        //将yellow:黄色... 分割
         *part1 = s.substr(0, pos);
    *part2 = s.substr(pos+1);
        return true ; 
    }

class Init
{
    public:
   
    Init()
    {
            std::ifstream in(dictname);//打开文件
            if(  in.is_open( )   == false )  //打开失败 
            {
    lg(Fatal, "ifstream open %s error", dictname.c_str());
            exit(1);

            }
        
        std::string str ;
        while(getline( in, str))
        {
            //std::cout<<getline( in, str);
             std::string part1, part2;
             Split(str, &part1, &part2);
                dict.insert( {part1, part2}) ;
        }



            //关闭文件
              in.close();
    }
        std::string translation(const std::string &key)
        {
            auto iterator   = dict.find( key) ;
            //没找到
            if(iterator == dict.end()) return  "Unkown"  ; 
            else   return  iterator->second ; 
        }


    private:
    std::unordered_map<std::string ,std::string >  dict ;//blue  , 蓝色
};