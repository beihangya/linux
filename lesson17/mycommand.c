#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>
int main()
{
  extern char ** environ;
  putenv("PRIVALUE_ENV=66666");
  pid_t id = fork(  ) ;
  if(id ==0 )
  {
    //child 
// char * const myargv[]= {"ls" , "-l","-a" ,NULL}  ;  
printf( " before : I am a process , pid %d , ppid %d\n", getpid () , getppid() ) ;
//sleep(5);
//最后一个参数必须是NULL
//execl ( "/usr/bin/ls" , "ls" , "-a","-l", NULL );  
//execlp ( "ls" , "ls" , "-a","-l", NULL );  
//execv ( "/usr/bin/ls" , myargv ); 

//execvp ( "ls" , myargv ); 

 char * const  myargv[]  = {  "otherExe" , "-a" , "-b" ,"-c" ,NULL };
char * const myenv[]={"MYVAL=1111" ,  "MYPATH=/usr/bin/xxx" , NULL};

execle ( "./otherExe", "otherExe" ,"-a", "-w","-v" , NULL ,myenv );
//execle ( "./otherExe", "otherExe" ,"-a", "-w","-v" , NULL ,environ );
// execv ("./otherExe",  myargv);
printf( " after : I am a process , pid %d , ppid %d\n", getpid () , getppid() ) ;
    exit(1);
  }
  //father 
  pid_t ret = waitpid(id , NULL, 0);
  if(ret > 0 )
  {
    printf("wait success, father pid :%d  , ret id:%d\n", getpid() , ret);
  }
  sleep(5);
  return 0 ;
}
