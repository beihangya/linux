#include" comm.hpp"

int main()
{
  //创建管道
  Init init;



  //processa进程创建共享内存
   int shmid= CreateShm();
 

//挂接
    char * shmaddr = (char*)shmat(shmid, nullptr , 0);
   

//进程通信

 // 一旦有人把数据写入到共享内存，其实我们立马能看到了， 不需要经过系统调用，直接就能看到数据了！
struct shmid_ds shmds;


//管道
int fd = open(FIFO_FILE, O_RDONLY); // 等待写入方打开之后，自己才会打开文件，向后执行， open 阻塞了！
    if (fd < 0)
    {
        log(Fatal, "error string: %s, error code: %d", strerror(errno), errno);
        exit(FIFO_OPEN_ERR);
    }



  while(true)
    {
        char ch ;
          ssize_t s = read(fd,&ch,1);

         if(s == 0) 
         break;
        else if(s < 0) 
        break;


        cout << "client say@ " << shmaddr << endl; //直接访问共享内存

         sleep(1);

         //获得共享内存的属性
          shmctl(shmid, IPC_STAT, &shmds);//IPC_STAT，将与 shmid 关联的内核数据结构中的信息复制到 buf 指向的shmid_ds结构中
  
            cout << "shm size: " << shmds.shm_segsz << endl;  //共享内存的大小
        cout << "shm nattch: " << shmds.shm_nattch << endl; //共享内存的挂接数
        printf("shm key: 0x%x\n",  shmds.shm_perm.__key); //key
        cout << "shm mode: " << shmds.shm_perm.mode << endl;//共享内存的权限
       
    }





   //去关联
      shmdt(shmaddr);

    //删除共享内存
  shmctl(shmid, IPC_RMID ,nullptr);

  //关闭管道文件
  close(fd);
    return 0 ;
}