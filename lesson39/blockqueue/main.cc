#include"BlockQueue.hpp"
#include"Task.hpp"
#include <ctime>
void *Consumer(void *args)
{
BlockQueue<Task> * bq = (BlockQueue<Task> *)args;

    while(true)
    {
        Task  t =bq->pop();
     

          t.run() ;
    std::cout << "处理任务: " << t.GetTask() << " 运算结果是： " << t.GetResult() << " thread id: " << pthread_self() << std::endl;
   
    }

}
void *Productor(void *args)
{
BlockQueue<Task> * bq = (BlockQueue<Task> *) args;
    int len = opers.size() ;
  
    while(true)
    {
             int data1 = rand() % 10 + 1; // [1,10]
        usleep(10);
        int data2 = rand() % 10;
        char op = opers[rand() % len];
          Task t(data1, data2, op);
    bq->push(t) ;
        std::cout<<"生产了一个数据"<<std::endl;
        sleep(1);
    }

}

int main()
{
// //单个生产者和单个消费者
//  srand(time(nullptr));
//     BlockQueue<Task> * bq= new BlockQueue<Task>() ;
//     pthread_t c, p;
//             //创建生产者和消费者的线程
//         pthread_create(&c,nullptr , Consumer,bq);
//          pthread_create(&p,nullptr , Productor,bq);
//             //线程等待
//             pthread_join(c,nullptr);
//             pthread_join(p,nullptr);
//     delete bq ;


    //多个生产者和多个消费者
 srand(time(nullptr));
    BlockQueue<Task> * bq= new BlockQueue<Task>() ;
    pthread_t c[3], p[5];
          

          
        for (int i = 0; i < 3; i++)
    {
         //创建消费者的线程
        pthread_create(c + i, nullptr, Consumer, bq);
    }

    for (int i = 0; i < 5; i++)
    {
         //创建生产者的线程
        pthread_create(p + i, nullptr, Productor, bq);
    }

    for (int i = 0; i < 3; i++)
    {
        pthread_join(c[i], nullptr);
    }
    for (int i = 0; i < 5; i++)
    {
        pthread_join(p[i], nullptr);
    }
    delete bq;

    
    return 0 ;
}