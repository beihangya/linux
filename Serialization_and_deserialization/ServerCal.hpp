#pragma once
#include <iostream>
#include "Protocol.hpp"

enum
{
    Div_Zero = 1,
    Mod_Zero,
    Other_Oper
};
class ServerCal
{
public:
    ServerCal()
    {
    }
    ~ServerCal()
    {
    }

public:
    Response CalculatorHelper(const Request &req)
    {
        Response resp(0, 0);
        switch (req.op)
        {
        case '+':
            resp.result = req.x + req.y;
            break;
        case '-':
            resp.result = req.x - req.y;
            break;
        case '*':
            resp.result = req.x * req.y;
            break;
        case '/':
            if (req.y == 0)
            {
                resp.code = Div_Zero;
            }
            resp.result = req.x / req.y;
            break;
        case '%':
        {
            if (req.y == 0)
            {
                resp.code = Mod_Zero;
            }

            resp.result = req.x % req.y;
        }
        break;
        default:
            resp.code = Other_Oper;
            break;
        }
    }
    // "9\n"100 + 200\n"
    std::string Calculator(std::string &package)
    {

        // 判断是否为完整报文
        std::string content;
	std::cout << "package: " << package << std::endl;
	//sleep(10); 
        //解析信息  
        bool r = Decode(package, &content);
        if (!r) // 不是完整报文
           {
            lg(Warning , "ServerCal.hpp: not a complete message ");
             return "";
           }
        // 是完整报文  ，反序列化
        Request req;
        r =  req.DeSerialize(content);
        if (!r)
         {
            lg(Warning , " DeSerialize false  ");
             return "";
           }
            
        content = ""; //
        // 提供进行计算的服务
        Response resp = CalculatorHelper(req);
        resp.Serialize(&content);  // 序列化
        content = Encode(content); //   "9\n"100 + 200\n"
        return content ;
    }
};
